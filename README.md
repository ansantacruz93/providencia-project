# Providencia Repository!

This is a repository to view branches structure provided for technical test to **Full-Stack developer Challenge**
 [Repository link](https://gitlab.com/ansantacruz93/providencia-project)
# Chellenge:

**![](https://lh3.googleusercontent.com/mznmgidUr61L1Z1MWMcHB0UNEGGMY9IQqx3rOLC3GAwvVsy7IoVNELGdL2xkde-I8zmYMK9DEMV4xxAICex_870AXjl4scnmWtBnFyaYXlkANl63WxX0EWIimIaKTRCMScPds0GI)**
# Result:
## Tag V1.0.1
**![](https://lh3.googleusercontent.com/AiONtNuxjahr9qLt6GaFo0SU44J5XnONvpnHS8OINO22IZR8-ui9QFCqpGCh9LiisGFXa4tlg2P5n7F9cn_r5coed2IQDAcQ-MWZT3X58a1JeKKBjkrjbdfen_Xb8MJuH2SaCzeL)**
## Tag V1.1.0
**![](https://lh4.googleusercontent.com/ZT9hslCFJjUEovTfCS4SEsuX9CPGwzZk03NgvWm7G2Gl1Buo0C4txQuS8le8jvMi5LSzp9ic3n3QFlmq8AQlCxOLX5wTOjBpkwJLv_QxhUQTuK1WQVuETOlM_SNPsr7jCnnxpJI2)**

## Tag V1.2.0

**![](https://lh4.googleusercontent.com/XOyQZiqN83VaV3-214nwMkNbammQFqIE_U-gWaSEIf3asvuUtWun5Bpxw0HAaZ1ghFjolZc90And7mwPb_JE-kteowGwYB8ZxIt8H7IEgZ-_FFerx2Q4wA67H2boJnmN7v1okwzf)**
