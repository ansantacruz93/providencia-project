export class Andres {

    nombre: string; 
    edad: number;
    documentoIdentidad: string;
    email: string; 
    direccion: string;

    constructor(){
        this.nombre = 'Andrés Camilo Santacruz Borda';
        this.edad = 27;
        this.documentoIdentidad = '1010217413'
        this.email = 'ansantacruz93@gmail.com'
        this.direccion = 'Calle 123'
    }

}